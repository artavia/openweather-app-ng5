import { Component, OnInit, Input } from '@angular/core';
import { DatePipe, DecimalPipe } from '@angular/common';

@Component({
  selector: 'app-forecast',
  templateUrl: './forecast.component.html',
  styleUrls: ['./forecast.component.scss']
})
export class ForecastComponent implements OnInit {

  // @Input() selectedWeather;
  @Input('selectedWeather') selectedWeather;
  @Input('selectedCityList') selectedCityList;

  public imageUrl = "/assets/images/layout/new_owa_371x41.gif";

  constructor() { }

  ngOnInit() {
    // console.log( 'this.selectedWeather' , this.selectedWeather ); // null
  }

  noOpLink(e: Event){
    e.preventDefault();
  }

}
