// version 2 ~ async version
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';

import { Injectable } from '@angular/core';
import { Location, locations } from '../classes/data-model';

@Injectable()
export class CityService {

  constructor() { }

  // version 1 ~ synchronous version
  // getLocations(): Location[]{ 
  //   return locations; 
  // }
   
  // version 2 ~ async version
  getLocations(): Observable<Location[]>{ 
    return of( locations ); 
  } 

}
