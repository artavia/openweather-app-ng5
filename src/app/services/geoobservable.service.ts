import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';

@Injectable()
export class GeoobservableService {
  
  constructor() {}
  
  // experiment 1 - getCurrentPosition
  locationObservable = new Observable( (observer) => {
    
    if (navigator.geolocation){
      navigator.geolocation.getCurrentPosition( 
        (pos) => {
          // console.log( 'pos' , pos );
          observer.next(pos);
          observer.complete();
          return observer.unsubscribe();
        }
        , () => {
          // console.log( 'Position is not available' );
          observer.error( 'Position is not available' );
        }
        , { enableHighAccuracy: true }
      );
    }
    else {
      observer.error( 'geolocation not supported' );
    }

  } );

}
