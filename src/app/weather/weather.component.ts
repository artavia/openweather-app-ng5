import { Component, OnInit, Input } from '@angular/core';
// import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.scss']
})
export class WeatherComponent implements OnInit {

  // @Input() selectedWeather;
  @Input('selectedWeather') selectedWeather; 
  @Input('formatted_currentdate') formatted_currentdate; 
  @Input('formatted_sunrise') formatted_sunrise; 
  @Input('formatted_sunset') formatted_sunset; 
  @Input('formatted_dayofweek') formatted_dayofweek;
  
  public imageUrl = "/assets/images/layout/new_owa_371x41.gif";

  constructor() { }

  ngOnInit() {
    // console.log( 'this.selectedWeather' , this.selectedWeather ); // null
  }

  noOpLink(e: Event){
    e.preventDefault();
  }

}

// SEE THESE ON THE SUBJECT OF DATE PIPES
// https://v5.angular.io/guide/observables-in-angular
// https://v5.angular.io/guide/pipes
// https://v5.angular.io/api?type=pipe
// https://v5.angular.io/api/common/DatePipe