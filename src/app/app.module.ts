import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms'; // ... furnishes template driven directives such as ngModel and NgForm, as well as, form validators... 

import { AppComponent } from './app.component';
import { NavComponent } from './elements/nav/nav.component';
import { FooterComponent } from './elements/footer/footer.component';
import { WeatherComponent } from './weather/weather.component';
import { ForecastComponent } from './forecast/forecast.component';
import { FormWeatherComponent } from './elements/form-weather/form-weather.component';
import { FormForecastComponent } from './elements/form-forecast/form-forecast.component';

import { CityService } from './services/city.service';
import { OpenweatherService } from './services/openweather.service';
import { OwaApiKeyService } from './services/owa-api-key.service';
import { GeoobservableService } from './services/geoobservable.service';

import { RoutingModule } from './routing.module';

@NgModule({
  declarations: [
    AppComponent
    , NavComponent
    , FooterComponent
    , WeatherComponent
    , ForecastComponent
    , FormWeatherComponent
    , FormForecastComponent
  ],
  imports: [
    BrowserModule
    , FormsModule
    , HttpClientModule
    , RoutingModule
  ],
  providers: [
    CityService
    , OpenweatherService
    , OwaApiKeyService
    , GeoobservableService
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
