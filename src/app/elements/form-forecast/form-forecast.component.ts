// jolo stuff, too
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { from } from 'rxjs/observable/from';
import 'rxjs/add/observable/throw';

import { Component, OnInit } from '@angular/core';
// import { DatePipe } from '@angular/common';

import { Location } from '../../classes/data-model'; // import { locations } from '../../classes/data-model';

import { CityService } from '../../services/city.service';
import { OpenweatherService } from '../../services/openweather.service';
import { GeoobservableService } from '../../services/geoobservable.service';

@Component({
  selector: 'app-form-forecast',
  templateUrl: './form-forecast.component.html',
  styleUrls: ['./form-forecast.component.scss']
})
export class FormForecastComponent implements OnInit {

  locations: Location[]; // versions 1 and 2
  selectedValue = null;
  myChosenValue = null;
  selectedWeather= null;
  selectedCityList = null;

  // jolo stuff, too
  currentLat: any;
  currentLong: any;
  singleObservable$: Observable<{}>;
  singleSubscription: Subscription;
  boundSinglePosition: any;

  constructor( private citysvc: CityService , private openweathersvc: OpenweatherService, public geosvc: GeoobservableService ) {
  }

  ngOnInit(){
    this.getLocations(); 
  }
  
  // version 1 ~ synchronous version
  // getLocations(): void { 
  //   this.locations = this.citysvc.getLocations(); 
  // } 

  // version 2 ~ async version
  getLocations(): void { 
    this.citysvc.getLocations().subscribe( ( locations ) => {
      this.locations = locations;
    } );
  }

  onForecastChange( newValue:any ): void {
    // console.log( 'newValue' , newValue );
    this.selectedValue = newValue;
    this.ifNotNullForecast( this.selectedValue );
  }

  ifNotNullForecast( pm ){
    if( pm !== null ){
      const myValue = pm.name;
      this.myChosenValue = myValue;
      this.runChangeForecast( pm.id );
    }
    else
    if( pm === null ){
      this.selectedWeather = null;
    }
  }

  runChangeForecast( id: number ){
    this.openweathersvc.selectForecastData( id ).subscribe( ( data ) => { 

      this.selectedCityList = data.list.filter( function(el) {
        return ( el.dt_txt.includes( '12:00:00' ) );    // for normal browsers
        // return ( el.dt_txt.indexOf( '12:00:00' ) !== -1 ); // for benefit of IE11 and less
      } ); // console.log( 'this.selectedCityList' , this.selectedCityList );

      this.selectedWeather = data; 
      // this.initFormattingMultipleDates( this.selectedWeather );
    } );
  } 

  onForecastClick( e ):void {
    this.currentPositionForecast();
  }

  currentPositionForecast() {
    
    this.singleObservable$ = this.geosvc.locationObservable;

    const fromObs$ = from( this.singleObservable$ );
    this.singleSubscription = fromObs$.subscribe(
      (pos) => {
        // console.log( 'pos' , pos );
        this.boundSinglePosition = pos;
        this.currentLat = this.boundSinglePosition.coords.latitude;
        this.currentLong = this.boundSinglePosition.coords.longitude;
        this.runGeoForecast( this.currentLat , this.currentLong );
      }
      , (err) => {
        // console.warn( `ERROR: ${err}` );
        return Observable.throw(err);
      }
      , () => {
        // console.log( 'Your query has been completed' );
      }
    );
  } 

  runGeoForecast( lat: number, lon: number ){ 
    this.openweathersvc.geoForecastData( lat, lon ).subscribe( ( data ) => { 
      
      this.selectedCityList = data.list.filter( function(el) {
        return ( el.dt_txt.includes( '12:00:00' ) );    // for normal browsers
        // return ( el.dt_txt.indexOf( '12:00:00' ) !== -1 ); // for benefit of IE11 and less
      } ); // console.log( 'this.selectedCityList' , this.selectedCityList );

      this.selectedWeather = data; 
      // this.initFormattingMultipleDates( this.selectedWeather );
    } );
  } 

}
