// jolo stuff, too
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { from } from 'rxjs/observable/from';
import 'rxjs/add/observable/throw';

import { Component, OnInit } from '@angular/core';
// import { DatePipe } from '@angular/common';

import { Location } from '../../classes/data-model'; // import { locations } from '../../classes/data-model';

import { CityService } from '../../services/city.service';
import { OpenweatherService } from '../../services/openweather.service';
import { GeoobservableService } from '../../services/geoobservable.service';

@Component({
  selector: 'app-form-weather',
  templateUrl: './form-weather.component.html',
  styleUrls: ['./form-weather.component.scss']
})
export class FormWeatherComponent implements OnInit {

  locations: Location[]; // versions 1 and 2
  selectedValue = null;
  myChosenValue = null;
  selectedWeather= null;

  // jolo stuff, too
  currentLat: any;
  currentLong: any;
  singleObservable$: Observable<{}>;
  singleSubscription: Subscription;
  boundSinglePosition: any;

  // Date formatting... a not so QUICK anti-pattern, but it is dirty, hairy, slimy, and loathsome
  today: number;
  formatted_dayofweek: string;
  currentdate: number;
  formatted_currentdate: string;
  sunrise: number;
  formatted_sunrise: string;
  sunset: number;
  formatted_sunset: string;

  constructor( private citysvc: CityService , private openweathersvc: OpenweatherService, public geosvc: GeoobservableService ) {
  }

  ngOnInit(){
    this.getLocations(); 
  }
  
  // version 1 ~ synchronous version
  // getLocations(): void { 
  //   this.locations = this.citysvc.getLocations(); 
  // } 

  // version 2 ~ async version
  getLocations(): void { 
    this.citysvc.getLocations().subscribe( ( locations ) => {
      this.locations = locations;
    } );
  }

  onWeatherChange( newValue:any ): void {
    // console.log( 'newValue' , newValue );
    this.selectedValue = newValue;
    this.ifNotNullWeather( this.selectedValue );
  }

  ifNotNullWeather( pm ){
    if( pm !== null ){
      const myValue = pm.name;
      this.myChosenValue = myValue;
      this.runChangeWeather( pm.id );
    }
    else
    if( pm === null ){
      this.selectedWeather = null;
    }
  }

  runChangeWeather( id: number ){
    this.openweathersvc.selectWeatherData( id ).subscribe( ( data ) => { 
      this.selectedWeather = data; 
      this.initDateFormatting( this.selectedWeather );
    } );
  }

  onWeatherClick( e ):void {
    this.currentPositionWeather();
  }

  currentPositionWeather() {
    
    this.singleObservable$ = this.geosvc.locationObservable;

    const fromObs$ = from( this.singleObservable$ );
    this.singleSubscription = fromObs$.subscribe(
      (pos) => {
        // console.log( 'pos' , pos );
        this.boundSinglePosition = pos;
        this.currentLat = this.boundSinglePosition.coords.latitude;
        this.currentLong = this.boundSinglePosition.coords.longitude;
        this.runGeoWeather( this.currentLat , this.currentLong );
      }
      , (err) => {
        // console.warn( `ERROR: ${err}` );
        return Observable.throw(err);
      }
      , () => {
        // console.log( 'Your query has been completed' );
      }
    );
  } 

  runGeoWeather( lat: number, lon: number ){ 
    this.openweathersvc.geoWeatherData( lat, lon ).subscribe( ( data ) => { 
      this.selectedWeather = data; 
      this.initDateFormatting( this.selectedWeather );
    } );
  }

  initDateFormatting( selwth ){
    this.today = selwth.dt;
    this.currentdate = selwth.dt;
    this.sunrise = selwth.sys.sunrise;
    this.sunset = selwth.sys.sunset;

    this.singleDayStringer( this.today );
    this.dateStringer( this.currentdate );
    this.sunriseStringer( this.sunrise );
    this.sunsetStringer( this.sunset); 
  }

  singleDayStringer( unixtimestamp ){ 
    let nowObj = new Date();
    let tzoffset = -( nowObj.getTimezoneOffset() ); 
    let timezoneoffset_milliseconds = tzoffset * 60 * 1000 ; 

    // let dateObj = new Date( unixtimestamp * 1000); 
    let dateObj = new Date( (unixtimestamp * 1000) + timezoneoffset_milliseconds ); 
    let arr_days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday']; 
    let day = arr_days[dateObj.getUTCDay()]; 
    let timestring = `${day}`;
    return this.formatted_dayofweek = timestring;
  }

  dateStringer( unixtimestamp ){ 
    let nowObj = new Date();
    // getTimezoneOffset() returns the time difference between UTC time and local time in minutes.
    let tzoffset = -( nowObj.getTimezoneOffset() ); // -360 (in minutes) eq. to -6 hours from GMT
    // this returns getTimezoneOffset in seconds (* 60 ), then, in milliseconds (* 1000 )
    let timezoneoffset_milliseconds = tzoffset * 60 * 1000 ; // -21600000 milliseconds

    // let dateObj = new Date( unixtimestamp * 1000); 
    let dateObj = new Date( (unixtimestamp * 1000) + timezoneoffset_milliseconds ); 

    let arr_months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];

    let month = arr_months[dateObj.getUTCMonth()]; 
    let date = dateObj.getUTCDate(); 
    let year = dateObj.getUTCFullYear(); 
    let hours = dateObj.getUTCHours(); 
    let minutes = dateObj.getUTCMinutes(); 
    let seconds = dateObj.getUTCSeconds(); 

    let amPm = hours >= 12 ? 'pm' : 'am'; 
    let hourstostring = ( "0" + ( ( hours % 12 ) || 12 ) ).substr(-2);
    let minutestostring = ("0" + minutes).substr(-2);
    let secondstostring = ("0" + seconds).substr(-2);    
    let timestring = `${month} ${date}, ${year} at ${hourstostring}:${minutestostring}:${secondstostring} ${amPm}`;
    return this.formatted_currentdate = timestring;
  }

  sunriseStringer( unixtimestamp ){
    let nowObj = new Date();
    // getTimezoneOffset() returns the time difference between UTC time and local time in minutes.
    let tzoffset = -( nowObj.getTimezoneOffset() ); // -360 (in minutes) eq. to -6 hours from GMT
    // this returns getTimezoneOffset in seconds (* 60 ), then, in milliseconds (* 1000 )
    let timezoneoffset_milliseconds = tzoffset * 60 * 1000 ; // -21600000 milliseconds

    // let dateObj = new Date( unixtimestamp * 1000); 
    let dateObj = new Date( (unixtimestamp * 1000) + timezoneoffset_milliseconds ); 

    let hours = dateObj.getUTCHours(); 
    let minutes = dateObj.getUTCMinutes(); 
    let seconds = dateObj.getUTCSeconds(); 

    let amPm = hours >= 12 ? 'pm' : 'am'; 
    let hourstostring = ( "0" + ( ( hours % 12 ) || 12 ) ).substr(-2);
    let minutestostring = ("0" + minutes).substr(-2);
    let secondstostring = ("0" + seconds).substr(-2);    
    let timestring = `${hourstostring}:${minutestostring}:${secondstostring} ${amPm}`;
    return this.formatted_sunrise = timestring;
  }

  sunsetStringer( unixtimestamp ){
    let nowObj = new Date();
    // getTimezoneOffset() returns the time difference between UTC time and local time in minutes.
    let tzoffset = -( nowObj.getTimezoneOffset() ); // -360 (in minutes) eq. to -6 hours from GMT
    // this returns getTimezoneOffset in seconds (* 60 ), then, in milliseconds (* 1000 )
    let timezoneoffset_milliseconds = tzoffset * 60 * 1000 ; // -21600000 milliseconds

    // let dateObj = new Date( unixtimestamp * 1000); 
    let dateObj = new Date( (unixtimestamp * 1000) + timezoneoffset_milliseconds ); 
    
    let hours = dateObj.getUTCHours(); 
    let minutes = dateObj.getUTCMinutes(); 
    let seconds = dateObj.getUTCSeconds(); 

    let amPm = hours >= 12 ? 'pm' : 'am'; 
    let hourstostring = ( "0" + ( ( hours % 12 ) || 12 ) ).substr(-2);
    let minutestostring = ("0" + minutes).substr(-2);
    let secondstostring = ("0" + seconds).substr(-2);    
    let timestring = `${hourstostring}:${minutestostring}:${secondstostring} ${amPm}`;
    return this.formatted_sunset = timestring;
  }

}
