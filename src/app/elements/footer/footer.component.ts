import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  dateObj: Date;
  myPresentYear: number;

  constructor() { }

  ngOnInit() {
    this.printYear();
  }

  printYear(): void {
    let newDate = new Date();
    this.dateObj = newDate;
    let year = this.dateObj.getFullYear();
    this.myPresentYear = year;
  }

}
