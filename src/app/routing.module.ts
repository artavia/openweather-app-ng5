import { NgModule } from '@angular/core';
import { RouterModule , Routes } from '@angular/router';

import { FormWeatherComponent } from './elements/form-weather/form-weather.component';
import { FormForecastComponent } from './elements/form-forecast/form-forecast.component';

const routes: Routes = [
  { path: '', redirectTo: '/weather', pathMatch: 'full' }
  , { path: 'weather', component: FormWeatherComponent }
  , { path: 'forecast', component: FormForecastComponent }
];

@NgModule({
  
  imports: [
    RouterModule.forRoot( routes )
  ]
  , exports: [
    RouterModule
  ]
  // , declarations: []

})
export class RoutingModule { }
