# Description
Fun with Angular v5, observables, and the OpenWeatherMap API.

## What it has
It makes limited use of the Geolocation API by utilizing the getCurrentPosition method. You will find that there ~~is code~~ are page components for each of the **forecast** and **weather** calls to the OpenWeatherMap API. I focused on the weather response as I began to build up the project because that response could be managed rather easily. Ironically, I used pipe expressions for outputting the **five day forecast** response but chose not to do so as I composed the **single day weather** component. Both of the responses are somewhat different and both are now included thanks to the use of the **angular routing module**.

## Please visit
You can see [the lesson at surge.sh](https://openweather-app-ng5.surge.sh "the lesson at surge") and decide if this is something for you to play around with at a later date.

## Or, just visit Angular 8 version of the project today
You can see [the lesson at surge.sh](https://ng8-owa-bulma-app.surge.sh "the lesson at surge") and decide if this is something for you to play around with at a later date.

## The backstory
This is a rehash of a pre&#45;existing project I had built for use with the Yahoo Weather API for use with php. That was way back in 2012. I refactored it for use, on the other hand, with Angular 5. The css styles are complicated and they were not trifled with. Yes, it can be hard on the eyes but my **true objective** is not to extrapolate further on styles and tidy them up in the process. Rather, my **focus** is on working with the **Reactive JS observables** that come with Angular. For those of you who are alert, there are some references to One Piece in the css styles, as well as, in the html code interspersed throughout. 

Thus, I can think of two (and, possibly more) **todo items** from the get go:

  - go back and clean up the css and html and scrub all references to One Piece;
  - fix the css specificity because IE8 and less are no longer considerations (this is why my default styles are not for mobile devices);
  - go back and replace and/or upgrade the geolocation button so that it includes love-hate functionality;
  - (possibly) go back and build out classes and/or interfaces as they relate to each the **weather** and **forecast** responses for more practical data binding.

## The companion project is in the works.
I also plan-- Lord permitting --on composing and releasing an identical project for use with ReactJS (or Create React App to be more accurate) within the next few days or week(s). The last time I had worked with something in ReactJS was around the end of April 2018 so that is something I can look forward to do in the short run. In other words, I have been working mostly with Angular v5 for the better part of two (2) months.

## Purpose of this toy project
I have had some practice with Angular. As it happens, the latest version is Angular v6 but it requires NodeJS version 8. That said, and if my true objective is not so obvious to others, I want to get in as much practice with Angular v5 before I move on to Angular v6. I am pleased with the results and it took six (6) days from beginning to end (there were adjustments) to build and publish this project.

## My name is Luis
You can call me don Lucho &amp; I hope you have fun during the rest of your day building something cool to share with the rest of the world!